'use strict';

const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));
const app = require('./server.js'); 

describe('API endpoint', function() {

  it('should return response', function() {
    chai.request(app)
      .post('/api/getResponse')
      .send({
        "firstname":"Amit",
        "lastname":"Nagpal",
        "annualsalary":"2000000",
        "superrate":"9",
        "paymentstartdate":"01 March – 31 March"
      })
      .then(function(res) {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body.results).to.be.an('array');
      });
  });

  it('should return Not Found', function() {
    chai.request(app)
      .get('/api/getResponse')
      .then(function(res) {
        throw new Error('Wrong Path exists!');
      })
      .catch(function(err) {
        expect(err).to.have.status(404);
      });
  });
});
