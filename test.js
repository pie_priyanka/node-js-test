var sinon = require('sinon');
var sinonTest = require('sinon-test');
var test = sinonTest(sinon);
var controller = require('./controller')

describe('Main Test', function () {
    var req = {
        body: {
            "firstname": "Amit",
            "lastname": "Nagpal",
            "annualsalary": "2000000",
            "superrate": "9",
            "paymentstartdate": "01 March – 31 March"

        }
    }
    res = {};
    beforeEach(function () {
        res = {
            json: sinon.spy(),
            status: sinon.stub().returns({ json: sinon.spy() })
        };
    });
    it('should return response with success', test(function () {
        controller.create(req, res);
        sinon.assert.calledOnce(res.status);
    }));
});

