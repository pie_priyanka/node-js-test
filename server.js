var express = require("express");
var main = require('./service');
var bodyParser = require('body-parser');


var app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(function(req,res,next){

res.header("Access-Control-Allow-Origin","*");
res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
next();
});
var router = express.Router();
router.get('/',function(req,res){

res.json({"message":"welcome to our apis"}); 
});
router.post('/getResponse',main.response);
app.use('/api',router);
app.listen(5001);
module.exports = app;

