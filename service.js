  exports.response = function (reqObject, res) {
  var firstName = reqObject.body.firstName;
  var lastName = reqObject.body.lastName;
  var annualSalary = reqObject.body.annualSalary;
  var superRate = reqObject.body.superRate;
  var paymentStartDate = reqObject.body.paymentStartDate;


  
  var tax;
  var difference;
  var salary = annualSalary / 12;
  var grossIncome = Math.round(salary);
  
  if (grossIncome >= 0 && grossIncome <= 18200) {
    tax = 0;
  }  else if (grossIncome >= 18201 && grossIncome <= 87000) {
    difference = grossIncome - 18201;
    tax = difference * 0.19;
  } else if (grossIncome >= 87001 && grossIncome <= 180000) {
    difference = grossIncome - 87001;
    tax = 3572 + (difference * 0.325);
  }else{
   difference = grossIncome - 180001;
    tax = 54232 + (difference * 0.45);
  }
  res.send( {
    "name": firstName + " " + lastName,
    "payPeriod": paymentStartDate,
    "grossIncome": grossIncome,
    "incomeTax": Math.round(tax),
    "netIncome": Math.round(grossIncome - tax),
    "superAmount": grossIncome * superRate,
  });
}



