'use strict';

const main = require('./service');
const controller = {};

controller.create =  async (req, res) => {

    try {
        const reponse =  main.response(req.body);

        return res.status(200).json(reponse);
    } catch (error) {
        return res.status(500).json(error);
    }

};
module.exports = controller;